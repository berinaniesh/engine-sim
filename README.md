# Engine-Sim

A line by line rewrite of [Ange Yahi's](https://angeyaghi.com) [engine-sim](https://github.com/ange-yaghi/engine-sim) just to read the code. I might later rewrite it in some other language or add functionality to the code. 
